---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_vault.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---

### Use Vault for random number generation and hashing
https://www.vaultproject.io/api/system/tools.html

### How to use Vault's API (From CURL):
https://www.vaultproject.io/api/index.html

### How to work with policies and configuration
https://www.hashicorp.com/blog/codifying-vault-policies-and-configuration

### Where go gets its CAs from. This is particularly important in getting rabbit-mq working.
https://golang.org/src/crypto/x509/root_linux.go

### Alternatives to Vault
https://github.com/OpenVPN/easy-rsa

### AppRole authentication for Vault
https://blog.alanthatcher.io/vault-approle-authentication/

{% include links.html %}
