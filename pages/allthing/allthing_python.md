---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_python.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---

### Using decorators:
https://hackaday.com/2018/08/31/an-introduction-to-decorators-in-python

### Using generators:
https://hackaday.com/2018/09/19/learn-to-loop-the-python-way-iterators-and-generators-explained/

### Intro to Django:
https://opensource.com/article/18/8/django-framework

### Testing containers with Conu (Python):
https://fedoramagazine.org/test-containers-python-conu/

### Debugging python in VS Code:
code.visualstudio.com/docs/python/debugging

### Python/VS Code/Docker debugging example:
github.com/ChenTanyi/python-debug-docker
github.com/DonJayamanne/vscode-python-samples/tree/master/remote-debugging-docker

### Python's pdb(Debugger) docs:
docs.python.org/3/library/pdb.html

### How to find out where Python is pulling libraries from:
leemendelowitz.github.io/blog/how-does-python-find-packages.html

### VS Code Python settings:
code.visualstudio.com/docs/python/settings-reference

### I liked the Template example for replacing $values in strings:
stackoverflow.com/questions/3877623/in-python-can-you-have-variables-within-triple-quotes-if-so-how


{% include links.html %}
