---
title: Documentation 
last_updated: July 12, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_docs.html
folder: allthing
---

### Docs Like Code
https://www.docslikecode.com

### To add a page: 
1. Add page reference in _data/topnav.yml (Optional)
2. Add page reference in _data/sidebars/name_sidebar.yml
3. Create page in the sidebar's folder in pages/

### Embeddable Markdown editor (open source): 
[prose.io](http://prose.io)
[Pando](https://github.com/pandao/editor.md)

### Jekyll-Reload gem to reload page... Should already be working, but is not.
[Jekyll-Reload](https://www.rubydoc.info/gems/jekyll-reload/)

### How posts work
https://jekyllrb.com/docs/posts/

### Dynamic menus in Jekyll
https://thinkshout.com/blog/2014/12/creating-dynamic-menus-in-jekyll/

### How linking in Jekyll works
https://www.digitalocean.com/community/tutorials/controlling-urls-and-links-in-jekyll

### Pages on what formatting to use
http://ericholscher.com/blog/2016/mar/15/dont-use-markdown-for-technical-docs/
https://asciidoctor.org/docs/what-is-asciidoc/
http://www.sphinx-doc.org/en/stable/
https://www.makeuseof.com/tag/compare-markup-language-asciidoc-markdown/
https://news.ycombinator.com/item?id=11922485

### Markdown editors
https://itsfoss.com/best-markdown-editors-linux/
https://remarkableapp.github.io/linux.html

### Jekyll Themes
https://github.com/jekyll/jekyll/wiki/Themes
https://jekyllrb.com/docs/themes/
http://jekyllthemes.org/
https://jekyllthemes.io/

### Some information about configuration and themes
https://www.taniarascia.com/make-a-static-website-with-jekyll/

### Original repo
https://github.com/tomjoht/documentation-theme-jekyll

### Hosting Jekyll on Github with SSL
https://hackernoon.com/how-to-setup-your-jekyll-website-with-free-web-hosting-ssl-and-a-custom-domain-4056ff862ca1
https://programminghistorian.org/en/lessons/building-static-sites-with-jekyll-github-pages
https://help.github.com/articles/configuring-jekyll/

{% include links.html %}


