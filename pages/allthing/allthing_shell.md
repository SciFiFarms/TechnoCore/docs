---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_shell.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---

### How to do debug levels in bash:
https://stackoverflow.com/questions/42403558/how-do-i-manage-log-verbosity-inside-a-shell-script

### CLI Improved: Pretty replacements for common cli tools. With aliases. 
https://remysharp.com/2018/08/23/cli-improved

### Bish-bosh is a shell mqtt client... That I didn't get working. 
https://github.com/raphaelcohn/bish-bosh

### How to write more compact bash. What chars are needed where and why:
https://www.linuxjournal.com/content/writing-more-compact-bash-code

### Bash variable scope examples:
https://www.thegeekstuff.com/2010/05/bash-variables/

### Use expect to dynamically enter information on the command line:
https://askubuntu.com/questions/269878/how-to-use-expect-with-bash
https://stackoverflow.com/questions/4780893/use-expect-in-bash-script-to-provide-password-to-ssh-command
https://stackoverflow.com/questions/681928/how-can-i-make-an-expect-script-prompt-for-a-password
https://likegeeks.com/expect-command/
https://stackoverflow.com/questions/945998/enter-password-multiple-times

### Setting vi mode. Seems REALLY useful to learn: set -o vi
https://unix.stackexchange.com/questions/30454/advantages-of-using-set-o-vi

### Using find:
https://blog.lerner.co.il/a-quick-intro-to-the-unix-find-utility/

### Bash OO(Infinity?) Framework
https://github.com/niieani/bash-oo-framework?utm_source=DevOps%27ish&utm_campaign=b6b758ef9c-088&utm_medium=email&utm_term=0_eab566bc9f-b6b758ef9c-46451983

### Bash is a type of sh:
https://stackoverflow.com/questions/5725296/difference-between-sh-and-bash

### Try command until it works:
https://stackoverflow.com/questions/5274294/how-can-you-run-a-command-in-bash-over-until-success

### Reading ENV and setting default values (${VAR_A:-default_value}):
https://unix.stackexchange.com/questions/122845/using-a-b-for-variable-assignment-in-scripts/122878

### Example of how to use cut to get specific fields.
https://unix.stackexchange.com/questions/53310/splitting-string-by-the-first-occurrence-of-a-delimiter
#### Can use ref to cut fields from the end:
https://stackoverflow.com/questions/4563060/how-to-cut-the-last-field-from-a-shell-string

### Why use #!/usr/bin/env bash 
https://unix.stackexchange.com/questions/29608/why-is-it-better-to-use-usr-bin-env-name-instead-of-path-to-name-as-my

### Shellfire is a generic shell framework. Seems pretty cool. 
https://github.com/shellfire-dev/shellfire

### Graphqurl (Shellfire GraphQL/CURL. Yeah, doesn't make a lot of sense.)
https://github.com/hasura/graphqurl

### Echo output to stderr >2&
https://stackoverflow.com/questions/2990414/echo-that-outputs-to-stderr

### Bash string substitution
https://stackoverflow.com/questions/13210880/replace-one-substring-for-another-string-in-shell-script

### Use ping to check if service is up
https://unix.stackexchange.com/questions/190163/shell-command-script-to-see-if-a-host-is-alive

### Check if variable is set
https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash

### Also see the Linux page.


{% include links.html %}
