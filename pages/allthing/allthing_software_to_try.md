---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_software_to_try.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---


### TMux
https://linuxize.com/post/getting-started-with-tmux

### Slack alternatives: MatterMost vs Riot. Lean towards MatterMost, but I like Riot's security model.
https://www.slant.co/versus/20863/12763/~riot_vs_mattermost
https://www.reddit.com/r/selfhosted/comments/7k471o/zulip_vs_rocketchat_vs_mattermost_vs_riotim_slack/
https://medium.com/ignation/time-to-replace-slack-who-will-win-mattermost-or-riot-matrix-a090e9cdc219

### Great list of what CI solutions are out there, and if they are open source or not
https://github.com/ligurio/awesome-ci

### Wekan is a cool open source version of trello... But actually, it seems to have integrations with NextCloud and GitLab/GitHub
https://wekan.github.io/

### Going Google Free:
https://www.reddit.com/r/LineageOS/comments/8xvzje/going_googlefree_went_better_than_expected/

### OnlyOffice
https://medium.com/onlyoffice/onlyoffice-or-collabora-pt-2-3f5599b1d742

### GitLab
https://about.gitlab.com/pricing/#self-hosted

### BulletNotes 
https://gitlab.com/NickBusey/BulletNotes
https://abhishekdas.com/HackFlowy/#
https://workflowy.com/demo/embed/

### Standard Notes (With encryption!)
https://opensource.com/article/18/8/getting-started-standard-notes

### Emacs?
https://notmyfirslanguage.writeas.com/the-road-to-emacs

### Bash OO(Infinity?) Framework
https://github.com/niieani/bash-oo-framework?utm_source=DevOps%27ish&utm_campaign=b6b758ef9c-088&utm_medium=email&utm_term=0_eab566bc9f-b6b758ef9c-46451983

### Logwatch (and tutorial)
https://www.techrepublic.com/article/how-to-install-and-use-logwatch-on-linux/

### WolfMQTT - It has an arduino build... Can it do an ESP?
https://github.com/wolfSSL/wolfMQTT

### Gnome Shell Android Integretaion
https://www.linuxuprising.com/2018/08/gnome-shell-android-integration.html

### Lightscreen (Greenshot alternative?)
http://lightscreen.com.ar/

### When to use REST, GraphGL, Webhook-grpc
https://nordicapis.com/when-to-use-what-rest-graphql-webhooks-grpc/

### Gitpod (Online IDE for github)
https://medium.com/gitpod/gitpod-gitpod-online-ide-for-github-6296b907a886

### Android Things (IoT framework?)
https://developer.android.com/things/versions/things-1.0

### Guacamole: X windows to HTML5 for containers. 
https://blog.openshift.com/put-ide-container-guacamole/?utm_medium=Email&utm_campaign=editorspicks&sc_cid=701f2000001OCwtAAG

### Gaia: Flexible pipeline creator
https://github.com/gaia-pipeline/gaia

### CLI Improved: Prettier and more useful cli programs:
https://remysharp.com/2018/08/23/cli-improved
{% include links.html %}
