---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---

### Getting started with Kubernetes
https://vsupalov.com/getting-started-with-kubernetes/

### Logging Kubernetes
https://itnext.io/logging-best-practices-for-kubernetes-using-elasticsearch-fluent-bit-and-kibana-be9b7398dfee

### Kubernetes cheatsheet
https://developers.redhat.com/promotions/kubernetes-cheatsheet/

### Monitoring Kubernetes with Prometheus
https://sysdig.com/blog/kubernetes-monitoring-prometheus/#.W3bhp8LlaW8.hackernews
https://www.robustperception.io/deleting-time-series-from-prometheus

### Kubernetes Cheatsheet
https://developers.redhat.com/promotions/kubernetes-cheatsheet/

### Helm: K8s Package management (Most used tool)
https://www.digitalocean.com/community/tutorials/an-introduction-to-helm-the-package-manager-for-kubernetes
https://www.weave.works/blog/kubernetes-anti-patterns-let-s-do-gitops-not-ciops

### Stateful Kubernetes
https://stateful.kubernetes.sh/

{% include links.html %}
