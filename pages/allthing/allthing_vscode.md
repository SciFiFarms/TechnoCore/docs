---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_vscode.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---

### Browser Sync:
https://marketplace.visualstudio.com/items?itemName=jasonlhy.vscode-browser-sync

### Turn off autoReveal in the file explorer
https://github.com/Microsoft/vscode/issues/5329

{% include links.html %}
