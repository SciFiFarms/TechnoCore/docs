---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_git.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---
### Git commit workflow (Haven't read):
https://dev.to/shreyasminocha/how-i-do-my-git-commits-34d?utm_campaign=DevOps%2BDispatch&utm_medium=email&utm_source=DevOps_Dispatch_11

### Git Subrepo 
https://github.com/ingydotnet/git-subrepo
#### FAQ includes how to change tracking branch
https://github.com/ingydotnet/git-subrepo/wiki/FAQ
#### Basics
https://github.com/ingydotnet/git-subrepo/wiki/Basics

### How to merge two git repos:
https://stackoverflow.com/questions/1425892/how-do-you-merge-two-git-repositories

### Make a file executable: git add --chmod=+x path/to/file
https://stackoverflow.com/questions/40978921/how-to-add-chmod-permissions-to-file-in-git/40979016


### Guide to moving subfolder into new repository
https://help.github.com/articles/splitting-a-subfolder-out-into-a-new-repository/

### Gitpod - Browser git IDE.
https://medium.com/gitpod/gitpod-gitpod-online-ide-for-github-6296b907a886

### GitOps FAQ
https://www.weave.works/blog/the-official-gitops-faq
https://www.weave.works/blog/kubernetes-anti-patterns-let-s-do-gitops-not-ciops

{% include links.html %}

### How to use github via ssh (Passwordless)
https://stackoverflow.com/questions/14762034/push-to-github-without-password-using-ssh-key
https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/
https://help.github.com/articles/which-remote-url-should-i-use/