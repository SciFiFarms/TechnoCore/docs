---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_startups.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---

### One DevOps stack and what the tools address
https://hackernoon.com/youre-not-paying-the-true-cost-of-software-development-and-it-s-killing-your-business-8d44b54be055

### 5 lessons learned working at startups
https://medium.com/@copyconstruct/top-5-lessons-learned-working-at-startups-828ba1d13aa4

### How to hire remote employees.
https://hackernoon.com/how-to-interview-the-best-remote-workers-88570ddce7d7

### Analysis of why 256 startups failed.
https://hackernoon.com/why-do-startups-fail-a-postmortem-of-256-failed-startups-fb157a16d98b


{% include links.html %}
