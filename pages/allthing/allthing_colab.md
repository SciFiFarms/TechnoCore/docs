---
title: Documentation 
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: allthing_sidebar
permalink: allthing_colab.html
folder: allthing
# Don't forget to add a reference in _data/sidebars/allthing_sidebar.yml and/or _data/topnav.yml 
---


### These people have some way of sharing data. 
https://www.uradmonitor.com/?mc_cid=0f4ca37c7a&mc_eid=a265f2cb07


infarm.de/#what-we-do

biggreen.org/where-were-growing/denver-front-range

hortidaily.com/article/43314/Aeroponic-greenhouse-opens-in-Turin

wctrib.com/business/agriculture/4440914-lettuce-abound-farms-serves-first-harvest-new-london-aeroponics

reddit.com/r/aeroponics/comments/8mjys7/update_my_homemade_vertical_garden

radicle.vc/the-radicle-challenge

astroplant.io/what-is-astroplant

community.home-assistant.io/t/my-docker-stack/43548/2

hortidaily.com/article/43684/The-quest-for-knowledge-in-indoor-farming-and-new-crops

hortidaily.com/article/43605/Give-employees-the-chance-to-get-shares-in-the-company

hortidaily.com/article/43694/The-need-to-optimise-growing-conditions-goes-beyond-just-light

Ponic (They share my vision):
ponics.tech/index.html

redhat.com/en/open-source-stories/collective-discovery

internetoflego.com

Graphic Design?
reddit.com/r/Art/comments/8m191i/hydroponic_garden_digital_720x720

hortidaily.com/article/42913/US-(WY)-Vertical-farm-in-Jackson-offers-fresh-produce-and-jobs-for-the-disabled

hortidaily.com/article/43058/Indian-horticultural-experts-gain-experience-in-the-Netherlands

wevolver.com/border.labs/astroplant/blob/master/documentation.md

indiehackers.com

freshtable.org

github.com/explore

openfarm.cc

independent.ie/business/farming/agri-business/companies/is-vertical-farming-the-new-growth-area-for-the-sector-36803060.html

People capturing carbon in dirt:
nytimes.com/2018/04/18/magazine/dirt-save-earth-carbon-farming-climate-change.html

Dat for sharing data:
dat.land

Similar farm:
livinggreensfarm.com/fresh-produce

Hydro farm in Loveland:
reporterherald.com/business/ci_31772118/lovelands-fyn-river-farms-prototype-grows-basil-nutrients

TimeScale - Growing in space:
timescale.eu/Pages/Partners.aspx

https://www.indigoag.com/pages/point-of-view/spotlight-jordan-lambert

thehindubusinessline.com/news/coimbatore-agri-engineer-grows-crops-out-of-thin-air-and-very-little-water/article23776834.ece

wur.nl/en/Education-Programmes/Current-Students/Student-Challenge/Could-you-design-the-ultimate-urban-greenhouse.htm

{% include links.html %}
