---
title:  
last_updated: July xx, 2018
summary: "Summary"
series: "ACME series"
weight: 3
sidebar: plants
permalink: plants_lighting.html
folder: plants
---

### THE BEST guide to building your own LED strip fixtures
http://ledgardener.com/diy-led-strip-build-designs-samsung-bridgelux/

### Lighting safety tips
https://www.reddit.com/r/HandsOnComplexity/comments/18fn67/space_bucket_electrical_safety_tips_posted_to/

### Hands on complexity
https://www.reddit.com/r/HandsOnComplexity/
https://www.reddit.com/r/HandsOnComplexity/comments/17nxhd/sags_plant_lighting_guide_linked_together/
https://www.reddit.com/r/HandsOnComplexity/comments/17nxpy/using_a_lux_meter_as_a_plant_light_meter/
https://www.reddit.com/r/HandsOnComplexity/comments/1mk7gm/led_and_led_grow_lights_part_3_color_and_white/

### Some stuff on calculating efficiency
https://www.reddit.com/r/SpaceBuckets/comments/5q7r0l/is_anyone_interested_in_learning_how_to_calculate/

### Comparison of crummy blurple vs COB
https://www.reddit.com/r/microgrowery/comments/6l7bh9/led_technology_sidebyside_comparison_grow_blurple/

### Comparing cost of running different lighting types
https://www.reddit.com/r/microgrowery/comments/81x1vp/a_note_to_all_cflt5t5ho_growers/

### Dimming LEDs with ESP8266
https://www.reddit.com/r/esp8266/comments/89f7f4/looking_for_a_wireless_010v_dimmer/

### Cutting aluminum 
https://www.wikihow.com/Cut-Aluminum

{% include links.html %}
